use nds2_client_rs::{
    Buffer,
    ChannelType,
    Data,
    DataType,
    GapHandler,
    NDSError,
    Parameters,
    Predicate,
    fetch,
    find_channels,
};

fn print_samples(b: &Buffer) {
    println!("{0} ({1} - {2}) ", b.channel.name, b.time.seconds, b.stop().seconds);

    let z: Result<Vec<i32>, _> = b.data.clone().try_into();
    match &z {
        Ok(v) => { print!("Int32 vec: "); v.iter().take(10).for_each(|e| {
            print!(" {0}", e);
        }); println!()},
        Err(_) => {print!("Not Int32\n")},
    }

    let z: Result<Vec<f32>, NDSError> = b.data.clone().try_into();
    match &z {
        Ok(v) => { print!("Float32 vec: "); v.iter().take(10).for_each(|e| {
            print!(" {0}", e);
        }); println!()},
        Err(_) => {print!("Not Float32\n")},
    }

    match &b.data {
        Data::Float32(v) => {
            v.iter().take(10).for_each(|e| {
                print!(" {0}", e);
            });
            println!();
        }
        Data::Int32(v) => {
            v.iter().take(10).for_each(|e| {
                print!(" {0}", e);
            });
            println!();
        }
        _ => println!("Skipping data")
    }
}

fn main() {
    let mut pred = Predicate::new();
    pred.pattern = String::from("H1:PEM*");
    pred.channel_type(ChannelType::Raw).data_type( DataType::Float32 );
    pred.max_sample_rate = 2048.0;
    let chans = find_channels(&pred).unwrap();
    chans.iter().for_each(|ch| {
        println!("{0}", ch)
    });
    let mut channels = Vec::new();
    channels.push(String::from("H1:FEC-28_CPU_METER.mean,m-trend"));
    let results = fetch(1391790650, 1391790656, &channels);
    match results {
        Ok(_) => println!("Got results back when it should have failed, how strange"),
        Err(e) => println!("Got an error, expecting a MinuteTrendGPSError {:?}", e),
    }

    let mut channels = Vec::new();
    channels.push(String::from("H1:FEC-28_CPU_METER"));
    let bufs = fetch(1391790650, 1391790656, &channels).unwrap();
    println!("Got {0} channels back", bufs.len());
    bufs.iter().for_each(print_samples);

    println!("repeating the query with a parameters block instead of defaults");
    let params = Parameters::new().gap_handler(GapHandler::NaN);
    let bufs = params.fetch(1391790650, 1391790656, &channels).unwrap();
    println!("Got {0} channels back", bufs.len());
    bufs.iter().for_each(print_samples);
}