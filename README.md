nds2_client_rs

A simplified test rust binding on the standalone NDS client functions.

This supports find_channels, iterate, and fetch currently.

This requires that at least the following be installed:

 * libclang
 * nds2-client dev files
 * rapidjson dev files

On Debian 12 as an example the following packages are used for development

 * libclang-common-14-dev
 * libclang-cpp14
 * libclang1-14
 * libclang-rt-14-dev
 * libnds2-client-dev
 * nds2-client-lib
 * rapidjson-dev

Major issues:

 * Not sure if the output structures are right
 * Not sure if the crate name is right
 