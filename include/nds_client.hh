#ifndef NDS2_CLIENT_RUST_WRAPPER_HH
#define NDS2_CLIENT_RUST_WRAPPER_HH

#include <nds.hh>
#include "rust/cxx.h"

#include <rapidjson/prettywriter.h>

template <typename Fail>
void
annotate_and_fail_nds(Fail &&fail, const char* exception_type, int id, const char* message)
{
    std::string msg{};
    try {
        rapidjson::StringBuffer sb{};
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(sb);
        writer.StartObject();
        writer.String("error_type");
        writer.String((exception_type?exception_type:"unknown"));
        writer.String("id");
        writer.Int(id);
        writer.String("message");
        writer.String((message?message:""));
        writer.EndObject();
        msg = sb.GetString();
    } catch(...) {
        fail(R"({"error_type": "unhandled", "id": -1, "message": "error while decoding exception"})");
        return;
    }
    fail(msg);
}

namespace rust {
    namespace behavior {

        template <typename Try, typename Fail>
        static void trycatch(Try &&func, Fail &&fail) noexcept
        try {
            func();
        }
        catch(NDS::connection::daq_error &e) { annotate_and_fail_nds(fail, "daq_error", e.DAQCode(), e.what()); }
        catch(NDS::connection::unexpected_channels_received_error &e) { annotate_and_fail_nds(fail, "unexpected_channels_received_error", -1, e.what()); }
        catch(NDS::connection::transfer_busy_error &e) { annotate_and_fail_nds(fail, "transfer_busy_error", -1, e.what()); }
        catch(NDS::connection::minute_trend_error &e) { annotate_and_fail_nds(fail, "minute_trend_error", -1, e.what()); }
        catch(NDS::connection::already_closed_error &e) { annotate_and_fail_nds(fail, "already_closed_error", -1, e.what()); }
        catch(NDS::connection::error &e) { annotate_and_fail_nds(fail, "error", -1, e.what()); }
        catch(std::exception &e) { annotate_and_fail_nds(fail, "cxx", -1, e.what()); }
        catch(...) {
            fail(R"({"error_type": "unknown", "id": -1, "message": ""})");
        }
    }
}

class Buffer;
class Channel;
class Predicate;
class GPSTime;

std::unique_ptr<NDS::parameters>
parameters_new();

std::unique_ptr<NDS::parameters>
parameters_with_hostinfo(const rust::Str hostname, std::int16_t port, std::int32_t protocol_version);

void parameters_set(NDS::parameters& param, const rust::Str key, const rust::Str value);

Channel nds_channel_to_rust(const NDS::channel& chan);

Channel nds_buffer_get_channel(const NDS::buffer& buf);

GPSTime nds_buffer_get_time(const NDS::buffer& buf);

inline std::size_t
nds_buffer_get_samples(const NDS::buffer& buf)
{
    return buf.Samples();
}

inline rust::Slice<const unsigned char>
nds_buffer_get_bytes(const NDS::buffer& buf)
{
    const unsigned char* start = reinterpret_cast<const unsigned char*>(buf.cbegin<void>());
    const unsigned char* end = reinterpret_cast<const unsigned char*>(buf.cend<void>());
    return rust::Slice<const unsigned char>(start, end-start);
}

inline bool
nds_iterator_is_done(NDS::data_iterable& nds_iter)
{
    return nds_iter.begin() == nds_iter.end();
}


void
nds_iterator_get_data(NDS::data_iterable& nds_iter, rust::Vec<Buffer>& out);

rust::Vec<Channel> find_channels_ext(const NDS::parameters& params, const Predicate& predicate);

void
fetch_ext(const NDS::parameters& params, std::uint64_t gps_start, std::uint64_t gps_end, const rust::Vec<rust::String>& channels, rust::Vec<Buffer>& out);

std::unique_ptr<NDS::data_iterable>
iterate_ext(const NDS::parameters& params, std::uint64_t start, std::uint64_t stop, std::int64_t stride, const rust::Vec<rust::String>& channels);

#endif // NDS2_CLIENT_RUST_WRAPPER_HH