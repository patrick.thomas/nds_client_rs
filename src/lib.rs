pub mod util;

use std::any::{Any, TypeId};
use std::cmp::Ordering;
use thiserror::Error;
use num_complex::{Complex32, Complex64};
use byte_slice_cast::*;
use cxx::{Exception, UniquePtr};
use serde::Deserialize;
use serde_json;

#[cxx::bridge]
mod ffi {
    #[derive(Debug, Hash)]
    enum ChannelType {
        Unknown,
        Online,
        Raw,
        RDS,
        STrend,
        MTrend,
        TestPoint,
        Static,
    }

    #[derive(Debug, Hash)]
    enum DataType {
        Unknown,
        Int16,
        Int32,
        Int64,
        Float32,
        Float64,
        Complex32,
        UInt32,
    }

    #[derive(Clone, Debug, PartialEq)]
    struct Channel {
        name: String,
        channel_type: ChannelType,
        data_type: DataType,
        sample_rate: f64,
        gain: f32,
        slope: f32,
        offset: f32,
        units: String,
    }

    struct Predicate {
        pattern: String,
        channel_types: Vec<ChannelType>,
        data_types: Vec<DataType>,
        min_sample_rate: f64,
        max_sample_rate: f64,
        gps_start: u64,
        gps_end: u64,
    }

    #[derive(Clone, Copy, Debug)]
    struct GPSTime {
        seconds: u64,
        nano: u64,
    }

    unsafe extern "C++" {
        include!("nds2_client_rs/include/nds_client.hh");

        #[namespace = NDS]
        type channel;

        #[namespace = NDS]
        type buffer;

        #[namespace = NDS]
        type data_iterable;

        #[namespace = NDS]
        type parameters;

        fn parameters_new() -> Result<UniquePtr<parameters>>;
        fn parameters_with_hostinfo(hostname: &str, port: i16, protocol_version: i32) -> Result<UniquePtr<parameters>>;
        fn parameters_set(params: Pin<&mut parameters>, key: &str, value: &str);

        fn nds_channel_to_rust(ch: &channel) -> Result<Channel>;
        fn nds_buffer_get_channel(buf: &buffer) -> Result<Channel>;
        fn nds_buffer_get_time(buf: &buffer) -> GPSTime;
        fn nds_buffer_get_samples(buf: &buffer) -> usize;
        fn nds_buffer_get_bytes(buf: &buffer) -> &[u8];
        fn nds_iterator_is_done(nds_iter: Pin<&mut data_iterable>) -> bool;
        fn nds_iterator_get_data(nds_iter: Pin<&mut data_iterable>, out: &mut Vec<Buffer>) -> Result<()>;

        fn find_channels_ext(params: &parameters, predicate: &Predicate) -> Result<Vec<Channel>>;

        fn fetch_ext(params: &parameters, gps_start: u64, gps_end: u64, channels: &Vec<String>, out: &mut Vec<Buffer>) -> Result<()>;

        fn iterate_ext(params: &parameters, start: u64, stop: u64, stride: i64, channels: &Vec<String>) -> Result<UniquePtr<data_iterable>>;
    }

    extern "Rust" {
        type Buffer;

        fn nds_buffer_append_to_rust_vec(buf: &buffer, output: &mut Vec<Buffer>) -> Result<()>;
    }
}

impl ffi::GPSTime {
    pub fn add_nano(&mut self, delta_nano: u64) {
        let one_billion = 1000000000;
        let mut delta_sec = delta_nano / one_billion;
        let delta_nano = delta_nano % one_billion;
        self.nano += delta_nano;
        if self.nano >= one_billion {
            delta_sec += self.nano / one_billion;
            self.nano %= one_billion;
        }
        self.seconds += delta_sec;
    }
}

impl PartialEq for ffi::GPSTime {
    fn eq(&self, other: &Self) -> bool {
        self.seconds == other.seconds && self.nano == other.nano
    }
}
impl Eq for ffi::GPSTime {}

impl Ord for ffi::GPSTime {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.seconds.cmp(&other.seconds) {
            Ordering::Less => Ordering::Less,
            Ordering::Equal => self.nano.cmp(&other.nano),
            Ordering::Greater => Ordering::Greater,
        }
    }
}

impl PartialOrd for ffi::GPSTime {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

/// Iterate over a discreet range of GPSTime entries
/// This is primarily used to provide a GPSTime stream
/// that can correspond to the data points in a Buffer.
#[derive(Clone)]
pub struct GPSTimeIterator {
    cur: GPSTime,
    steps: usize,
    step_size_nano: u64,
}

impl GPSTimeIterator {
    pub fn new(start: GPSTime, steps: usize, step_size_nano: u64) -> Self {
        Self{
            cur: start,
            steps,
            step_size_nano,
        }
    }
}

impl Iterator for GPSTimeIterator {
    type Item = GPSTime;

    fn next(&mut self) -> Option<Self::Item> {
        if self.steps > 0 {
            let result = self.cur;
            self.cur.add_nano(self.step_size_nano as u64);
            self.steps -= 1;
            Some(result)
        } else {
            None
        }
    }
}

fn nds_buffer_append_to_rust_vec(buf: &ffi::buffer, output: &mut Vec<Buffer>) -> Result<(), NDSError> {
    let chan = match ffi::nds_buffer_get_channel(buf) {
        Ok(ch) => ch,
        Err(_) => return Err(NDSError::InvalidInput),
    };
    let time = ffi::nds_buffer_get_time(buf);
    let samples = ffi::nds_buffer_get_samples(buf);
    let data = ffi::nds_buffer_get_bytes(buf);
    let buf = Buffer::new_from_bytes(chan, time, samples, data)?;
    output.push(buf);
    Ok(())
}

impl ffi::Predicate {
    pub fn new() -> Self {
        ffi::Predicate {
            pattern: String::from("*"),
            channel_types: Vec::new(),
            data_types: Vec::new(),
            min_sample_rate: 0.0,
            max_sample_rate: f64::MAX,
            gps_start: 0,
            gps_end: 1999999999,
        }
    }

    pub fn channel_type(&mut self, type_val: ChannelType) -> &mut Self {
        self.channel_types.push(type_val);
        self
    }

    pub fn data_type(&mut self, type_val: DataType) -> &mut Self {
        self.data_types.push(type_val);
        self
    }
}

impl Display for ffi::ChannelType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{0}", match *self {
            ChannelType::Online => "online",
            ChannelType::Raw => "raw",
            ChannelType::RDS => "rds",
            ChannelType::STrend => "s-trend",
            ChannelType::MTrend => "m-trend",
            ChannelType::TestPoint => "test-point",
            ChannelType::Static => "static",
            _ => "unknown",
        })
    }
}

impl Display for ffi::DataType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{0}", match *self {
            DataType::Int16 => "int16",
            DataType::Int32 => "int32",
            DataType::Int64 => "int64",
            DataType::Float32 => "float32",
            DataType::Float64 => "float64",
            DataType::Complex32 => "complex",
            DataType::UInt32 => "uint32",
            _ => "unknown",
        })
    }
}

impl Display for ffi::Channel {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{0} {1} {2} {3}>", self.name, self.sample_rate, self.data_type, self.channel_type)
    }
}

use std::fmt::{Display, Formatter};
use std::mem::{ManuallyDrop};

pub use ffi::{
    Channel,
    ChannelType,
    DataType,
    GPSTime,
    Predicate,
};

#[derive(Clone, Debug)]
pub enum Data {
    Int16(Vec<i16>),
    Int32(Vec<i32>),
    Int64(Vec<i64>),
    Float32(Vec<f32>),
    Float64(Vec<f64>),
    Complex32(Vec<Complex32>),
    UInt32(Vec<u32>),
    Unknown((Vec<u8>, usize)),
}

impl From<Vec<i16>> for Data {
    fn from(value: Vec<i16>) -> Self {
        Data::Int16(value)
    }
}

impl From<Vec<i32>> for Data {
    fn from(value: Vec<i32>) -> Self {
        Data::Int32(value)
    }
}

impl From<Vec<i64>> for Data {
    fn from(value: Vec<i64>) -> Self {
        Data::Int64(value)
    }
}

impl From<Vec<f32>> for Data {
    fn from(value: Vec<f32>) -> Self {
        Data::Float32(value)
    }
}

impl From<Vec<f64>> for Data {
    fn from(value: Vec<f64>) -> Self {
        Data::Float64(value)
    }
}

impl From<Vec<Complex32>> for Data {
    fn from(value: Vec<Complex32>) -> Self {
        Data::Complex32(value)
    }
}

impl From<Vec<u32>> for Data {
    fn from(value: Vec<u32>) -> Self {
        Data::UInt32(value)
    }
}

impl From<(Vec<u8>, usize)> for Data {
    fn from(value: (Vec<u8>, usize)) -> Self {
        Data::Unknown(value)
    }
}

impl TryFrom<Data> for Vec<i16> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::Int16(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting i16 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<i32> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::Int32(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting i32 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<i64> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::Int64(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting i64 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<f32> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::Float32(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting f32 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<f64> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::Float64(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting f64 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<Complex32> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::Complex32(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting Complex32 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<Complex64> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            _ => Err(NDSError::MismatchedTypesError(": Complex64 not yet supported".into())),
        }
    }
}

impl TryFrom<Data> for Vec<u16> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            _ => Err(NDSError::MismatchedTypesError(": u16 not yet supported".into())),
        }
    }
}

impl TryFrom<Data> for Vec<u32> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            Data::UInt32(v) => Ok(v),
            _ => Err(NDSError::MismatchedTypesError("when converting u32 Data enum into Vec".into())),
        }
    }
}

impl TryFrom<Data> for Vec<u64> {
    type Error = NDSError;

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        match value {
            _ => Err(NDSError::MismatchedTypesError(": u64 not yet supported".into())),
        }
    }
}

fn data_element_size(dt: DataType) -> usize {
    match dt {
        DataType::Int16 => 2,
        DataType::Int32 => 4,
        DataType::Int64 => 8,
        DataType::Float32 => 4,
        DataType::Float64 => 8,
        DataType::Complex32 => 4 + 4,
        DataType::UInt32 => 4,
        DataType::Unknown => 0,
        _ => 0,
    }
}

#[derive(Clone, Debug)]
pub struct Buffer {
    pub channel: Channel,
    pub time: GPSTime,
    pub data: Data,
}

#[derive(Deserialize)]
struct NDSErrorObject {
    error_type: String,
    id: i32,
    message: String,
}

#[derive(Error, Debug, Clone)]
pub enum NDSError {
    #[error("Function passed invalid input")]
    InvalidInput,
    #[error("Invalid or unknown data stream passed for a channel {0}")]
    InvalidChannelDataError(String),
    #[error("Unknown error while doing a fetch")]
    FetchError,
    #[error("Unknown error while doing an interate")]
    IterateError,
    #[error("Daqd/NDS Error {0}:{1}")]
    DaqdError(i32,String),
    #[error("Unable to resolve ip address from name")]
    InvalidIPAddressError,
    #[error("Invalid channel name")]
    InvalidChannelNameError,
    #[error("Unable to connect to specified host/port")]
    UnableToConnectError,
    #[error("The server is overloaded")]
    ServerBusyError,
    #[error("Data not found or channel does not exist {0}")]
    NotFoundError(String),
    #[error("Too many channels or too much data requested {0}")]
    TooManyChannelsError(String),
    #[error("Requested feature is not supported {0}")]
    NotSupportedError(String),
    #[error("Require SASL authentication {0}")]
    SaslAuthenticationError(String),
    #[error("Syntax error in request {0}")]
    SyntaxError(String),
    #[error("Requested data is on tape {0}")]
    DataOnTapeError(String),
    #[error("Access denied {0}")]
    AccessDeniedError(String),
    #[error("Unexpected channels received from the server")]
    UnexpectedChannelsReceivedError,
    #[error("A transfer is already in progress on this connection")]
    TransferBusyError,
    #[error("Minute trend requested, but start/stop GPS times are not divisible by 60")]
    MinuteTrendGPSError,
    #[error("The connection has already been closed")]
    ConnectionClosedError,
    #[error("General NDS error {0}")]
    GeneralNDSError(String),
    #[error("General Error in the nds2 library {0}")]
    GeneralNDSLibraryError(String),
    #[error("Mismatched types {0}")]
    MismatchedTypesError(String),
    #[error("Unknown error encountered in nds2 library")]
    UnknownError,
}

impl From<Exception> for NDSError {
    fn from(value: Exception) -> Self {
        let err_msg = value.what();
        if let Ok(err_obj) = serde_json::from_str::<NDSErrorObject>(err_msg) {
            // these strings must line up with the definitions in nds_client.hh
            match err_obj.error_type.as_str() {
                "daq_error" => match err_obj.id {
                    0x3 => NDSError::InvalidIPAddressError,
                    0x4 => NDSError::InvalidChannelNameError,
                    0x7 => NDSError::UnableToConnectError,
                    0x8 => NDSError::ServerBusyError,
                    0xd => NDSError::NotFoundError(err_obj.message),
                    0x16 => NDSError::TooManyChannelsError(err_obj.message),
                    0x17 => NDSError::NotSupportedError(err_obj.message),
                    0x18 => NDSError::SaslAuthenticationError(err_obj.message),
                    0x19 => NDSError::SyntaxError(err_obj.message),
                    0x1a => NDSError::DataOnTapeError(err_obj.message),
                    0x1b => NDSError::AccessDeniedError(err_obj.message),
                    _ => NDSError::DaqdError(err_obj.id, err_obj.message),
                },
                "unexpected_channels_received_error" => NDSError::UnexpectedChannelsReceivedError,
                "transfer_busy_error" => NDSError::TransferBusyError,
                "minute_trend_error" => NDSError::MinuteTrendGPSError,
                "already_closed_error" => NDSError::ConnectionClosedError,
                "error" => NDSError::GeneralNDSError(err_obj.message),
                "cxx" => NDSError::GeneralNDSLibraryError(err_obj.message),
                _ => NDSError::UnknownError,
            }
        } else {
            NDSError::UnknownError
        }
    }
}

fn bytes_to_data(data_type: DataType, samples: usize, bytes: &[u8]) -> Result<Data, NDSError> {
    let size = data_element_size(data_type);
    let d = if data_type != DataType::Unknown && size != 0 {
        match data_type {
            DataType::Int16 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<i16>().map_err(|_|NDSError::InvalidInput)?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::Int16(v)
            }
            DataType::Int32 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<i32>().map_err(|_|NDSError::InvalidInput)?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::Int32(v)
            }
            DataType::Int64 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<i64>().map_err(|_|NDSError::InvalidInput)?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::Int64(v)
            }
            DataType::Float32 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<f32>().map_err(|_|NDSError::InvalidInput)?;
                v.resize(samples, 0.0);
                v.copy_from_slice(b);
                Data::Float32(v)
            }
            DataType::Float64 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<f64>().map_err(|_|NDSError::InvalidInput)?;
                v.resize(samples, 0.0);
                v.copy_from_slice(b);
                Data::Float64(v)
            }
            DataType::Complex32 => {
                let b = bytes.as_slice_of::<f32>().map_err(|_|NDSError::InvalidInput)?;
                let i1 = b.iter().step_by(2);
                let i2 = b.iter().skip(1).step_by(2);
                let v = i1.zip(i2).map(|val| -> Complex32 {
                    Complex32::new(*val.0, *val.1)
                }).collect::<Vec<Complex32>>();
                Data::Complex32(v)
            }
            DataType::UInt32 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<u32>().map_err(|_|NDSError::InvalidInput)?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::UInt32(v)
            }
            _ => {
                return Err(NDSError::InvalidInput);
            }
        }
    } else {
        let mut v = Vec::new();
        v.resize(bytes.len(), 0);
        v.copy_from_slice(bytes);
        Data::Unknown((v, samples))
    };
    Ok(d)
}

impl Buffer {
    fn new_from_bytes(channel: Channel, time: GPSTime, samples: usize, bytes: &[u8]) -> Result<Buffer, NDSError> {
        let d = match bytes_to_data(channel.data_type, samples, bytes) {
            Ok(data) => data,
            Err(_) => return Err(NDSError::InvalidChannelDataError(channel.name)),
        };
        Ok(Buffer {
            channel,
            time,
            data: d,
        })
    }

    pub fn samples(&self) -> usize {
        match &self.data {
            Data::Int16(v) => v.len(),
            Data::Int32(v) => v.len(),
            Data::Int64(v) => v.len(),
            Data::Float32(v) => v.len(),
            Data::Float64(v) => v.len(),
            Data::Complex32(v) => v.len(),
            Data::UInt32(v) => v.len(),
            Data::Unknown((_, size)) => *size,
        }
    }

    pub fn start(&self) -> GPSTime {
        self.time.clone()
    }

    pub fn stop(&self) -> GPSTime {
        let samples = self.samples();
        let mut total_ns = self.time.nano + self.samples_to_trailing_nanoseconds(samples);
        let additional_from_nano = if total_ns >= 1000000000 {
            1
        } else {
            0
        };
        total_ns %= 1000000000;
        GPSTime {
            seconds: self.time.seconds + self.samples_to_seconds(samples) + additional_from_nano,
            nano: total_ns,
        }
    }

    fn samples_to_seconds(&self, count: usize) -> u64 {
        if let ChannelType::MTrend = self.channel.channel_type {
            return count as u64 * 60;
        }
        return (count as u64) / (self.channel.sample_rate as u64);
    }

    fn samples_to_trailing_nanoseconds(&self, count: usize) -> u64 {
        if let ChannelType::MTrend = self.channel.channel_type {
            return 0;
        }
        let samples = count % self.channel.sample_rate as usize;
        (1e+9 * samples as f64 / self.channel.sample_rate) as u64
    }

    pub fn append(&mut self, other: Buffer) -> Result<(), Buffer> {
        if self.channel.channel_type != other.channel.channel_type || self.channel.sample_rate != other.channel.sample_rate
            || self.channel.data_type != other.channel.data_type || self.channel.name != other.channel.name
            || self.stop() != other.time
        {
            return Err(other);
        }
        if let Ok(new_data) = combine_datum(self.data.clone(), other.data.clone()) {
            self.data = new_data;
            Ok(())
        } else {
            Err(other)
        }
    }

    /// Return an iterator that gives the GPSTime for each sample in the buffer
    pub fn time_iter(&self) -> GPSTimeIterator {
        let one_billion = 1000000000;
        let stride = if self.channel.channel_type == ChannelType::MTrend {
            60*one_billion
        } else {
            one_billion/(self.channel.sample_rate as u64)
        };
        GPSTimeIterator::new(self.time, self.samples(), stride)
    }
}

fn combine_datum(a: Data, b: Data) -> Result<Data, (Data, Data)> {
    match a {
        Data::Int16(mut va) => {
            if let Data::Int16(vb) = &b {
                va.extend(vb);
                Ok(Data::Int16(va))
            } else {
                Err((Data::Int16(va), b))
            }
        }
        Data::Int32(mut va) => {
            if let Data::Int32(vb) = &b {
                va.extend(vb);
                Ok(Data::Int32(va))
            } else {
                Err((Data::Int32(va), b))
            }
        }
        Data::Int64(mut va) => {
            if let Data::Int64(vb) = &b {
                va.extend(vb);
                Ok(Data::Int64(va))
            } else {
                Err((Data::Int64(va), b))
            }
        }
        Data::Float32(mut va) => {
            if let Data::Float32(vb) = &b {
                va.extend(vb);
                Ok(Data::Float32(va))
            } else {
                Err((Data::Float32(va), b))
            }
        }
        Data::Float64(mut va) => {
            if let Data::Float64(vb) = &b {
                va.extend(vb);
                Ok(Data::Float64(va))
            } else {
                Err((Data::Float64(va), b))
            }
        }
        Data::Complex32(mut va) => {
            if let Data::Complex32(vb) = &b {
                va.extend(vb);
                Ok(Data::Complex32(va))
            } else {
                Err((Data::Complex32(va), b))
            }
        }
        Data::UInt32(mut va) => {
            if let Data::UInt32(vb) = &b {
                va.extend(vb);
                Ok(Data::UInt32(va))
            } else {
                Err((Data::UInt32(va), b))
            }
        }
        Data::Unknown((mut va, asize)) => {
            if let Data::Unknown((vb, bsize)) = &b {
                va.extend(vb);
                Ok(Data::Unknown((va, asize + bsize)))
            } else {
                Err((Data::Unknown((va, asize)), b))
            }
        }
    }
}

pub fn find_channels(predicate: &Predicate) -> Result<Vec<Channel>, NDSError> {
    let params = Parameters::new();
    params.find_channels(predicate)
}

pub fn fetch(gps_start: u64, gps_end: u64, channels: &Vec<String>) -> Result<Vec<Buffer>, NDSError> {
    let params = Parameters::new();
    params.fetch(gps_start, gps_end, channels)
}

#[derive(Clone, Copy)]
pub enum Stride {
    Seconds(u32),
    Fast,
}

impl Stride {
    fn to_nds_stride(&self) -> i64 {
        match &self {
            Stride::Seconds(s) => *s as i64,
            Stride::Fast => -1,
        }
    }
}

pub struct NDSIterator {
    handle: UniquePtr<ffi::data_iterable>,
}

impl Iterator for NDSIterator {
    type Item = Vec<Buffer>;

    fn next(&mut self) -> Option<Self::Item> {
        if ffi::nds_iterator_is_done(self.handle.pin_mut()) {
            return None;
        }
        let mut out = Vec::new();
        match ffi::nds_iterator_get_data(self.handle.pin_mut(), &mut out) {
            Ok(_) => Some(out),
            Err(_) => None,
        }
    }
}

pub fn iterate(gps_start: u64, gps_end: u64, stride: Stride, channels: &Vec<String>) -> Result<NDSIterator, NDSError> {
    let params = Parameters::new();
    params.iterate(gps_start, gps_end, stride, channels)
}


#[derive(Debug,Clone)]
pub enum GapHandler {
    Abort,
    Zero,
    One,
    NaN,
    PosInf,
    NegInf,
}

#[derive(Debug,PartialEq,Clone)]
pub enum ProtocolVersion {
    One,
    Two,
    Auto,
}

impl From<ProtocolVersion> for i32 {
    fn from(value: ProtocolVersion) -> Self {
        match value {
            ProtocolVersion::One => 1,
            ProtocolVersion::Two => 2,
            ProtocolVersion::Auto => -1,
        }
    }
}

struct HostInfo {
    hostname: String,
    port: i16,
    protocol: ProtocolVersion,
}

struct ParametersInternals {
    host_info: Option<HostInfo>,
    gap_handler: Option<GapHandler>,
    allow_data_on_tape: Option<bool>,
    iterate_uses_gap_handler: Option<bool>
}

pub struct Parameters(ParametersInternals);
impl Parameters {
    pub fn new() -> Self {
        Self(ParametersInternals {
            host_info: None,
            gap_handler: None,
            allow_data_on_tape: None,
            iterate_uses_gap_handler: None,
        })
    }

    pub fn host_info(mut self, host_info: HostInfo) -> Self {
        self.0.host_info = Some(host_info);
        self
    }

    pub fn gap_handler(mut self, gap_handler: GapHandler) -> Self {
        self.0.gap_handler = Some(gap_handler);
        self
    }

    pub fn allow_data_on_tape(mut self, allow_data_on_tape: bool) -> Self {
        self.0.allow_data_on_tape = Some(allow_data_on_tape);
        self
    }

    pub fn iterate_uses_gap_handler(mut self, iterate_uses_gap_handler: bool) -> Self {
        self.0.iterate_uses_gap_handler = Some(iterate_uses_gap_handler);
        self
    }

    fn get_parameters(&self) -> Result<UniquePtr<ffi::parameters>, NDSError> {
        let bool_to_str = |value: bool| -> &'static str {
            match value {
                true => "true",
                false => "false",
            }
        };
        let mut p = match &self.0.host_info {
            Some(info) => {
                ffi::parameters_with_hostinfo(info.hostname.as_str(), info.port, info.protocol.clone().into()).map_err(|e| NDSError::from(e))?
            },
            None => { ffi::parameters_new().map_err(|e| NDSError::from(e))? },
        };
        if let Some(ref value) = self.0.allow_data_on_tape {
            ffi::parameters_set(p.pin_mut(), "ALLOW_DATA_ON_TAPE", bool_to_str(*value));
        }
        if let Some(ref value) = self.0.iterate_uses_gap_handler {
            ffi::parameters_set(p.pin_mut(), "ITERATE_USE_GAP_HANDLERS", bool_to_str(*value));
        }
        if let Some(ref value) = self.0.gap_handler {
            let val = match value {
                GapHandler::Abort => "ABORT_HANDLER",
                GapHandler::Zero => "STATIC_HANDLER_ZERO",
                GapHandler::One => "STATIC_HANDLER_ONE",
                GapHandler::NaN => "STATIC_HANDLER_NAN",
                GapHandler::PosInf => "STATIC_HANDLER_POS_INF",
                GapHandler::NegInf => "STATIC_HANDLER_NEG_INF"
            };
            ffi::parameters_set(p.pin_mut(), "GAP_HANDLER", val);
        }
        Ok(p)
    }

    // add an iterate, fetch, find_channels, ...
    pub fn find_channels(&self, predicate: &Predicate) -> Result<Vec<Channel>, NDSError> {
        let p = self.get_parameters()?;
        ffi::find_channels_ext(&*p, predicate).map_err(|e|NDSError::from(e))
    }

    pub fn fetch(&self, gps_start: u64, gps_end: u64, channels: &Vec<String>) -> Result<Vec<Buffer>, NDSError> {
        let p = self.get_parameters()?;
        let mut bufs = Vec::new();
        ffi::fetch_ext(&*p, gps_start, gps_end, channels, &mut bufs).map_err(|e| NDSError::from(e))?;
        Ok(bufs)
    }

    pub fn iterate(&self, gps_start: u64, gps_end: u64, stride: Stride, channels: &Vec<String>) -> Result<NDSIterator, NDSError> {
        let p = self.get_parameters()?;
        match ffi::iterate_ext(&*p, gps_start, gps_end, stride.to_nds_stride(), channels) {
            Ok(handle) => Ok(NDSIterator {
                handle
            }),
            Err(e) => Err(NDSError::from(e)),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn sample_buffer1_mtrend() -> Buffer {
        Buffer {
            channel: Channel {
                name: "X1:TEST_1".to_string(),
                channel_type: ChannelType::MTrend,
                data_type: DataType::Int32,
                sample_rate: 1.0 / 60.0,
                gain: 1.0,
                slope: 1.0,
                offset: 0.0,
                units: "counts".to_string(),
            },
            time: GPSTime { seconds: 1000000000, nano: 0 },
            data: Data::from(vec![1i32, 2i32, 3i32]),
        }
    }

    fn sample_buffer1_raw() -> Buffer {
        let mut data = Vec::new();
        for i in 0..32 {
            data.push(i as i32);
        }
        Buffer {
            channel: Channel {
                name: "X1:TEST_1".to_string(),
                channel_type: ChannelType::Raw,
                data_type: DataType::Int32,
                sample_rate: 16.0,
                gain: 1.0,
                slope: 1.0,
                offset: 0.0,
                units: "counts".to_string(),
            },
            time: GPSTime { seconds: 1000000000, nano: 0 },
            data: Data::from(data),
        }
    }

    #[test]
    fn test_buffer_append_fail()
    {
        let mut b1 = sample_buffer1_mtrend();

        assert!(b1.append(sample_buffer1_mtrend()).is_err());
        assert!(b1.append(sample_buffer1_raw()).is_err());

        let mut b2 = sample_buffer1_mtrend();
        b2.time.seconds += 5 * 60;
        assert!(b1.append(b2).is_err());
    }

    #[test]
    fn test_buffer_append_ok()
    {
        let mut b1 = sample_buffer1_mtrend();

        assert_eq!(b1.samples(), 3);
        assert_eq!(b1.start().seconds, 1000000000);
        assert_eq!(b1.start().nano, 0);
        assert_eq!(b1.stop().seconds, 1000000000 + 3 * 60);
        assert_eq!(b1.stop().nano, 0);

        let mut b2 = sample_buffer1_mtrend();
        b2.time.seconds += 3 * 60;
        match b1.append(b2) {
            Ok(_) => {
                assert_eq!(b1.samples(), 6);
                assert_eq!(b1.start().seconds, 1000000000);
                assert_eq!(b1.start().nano, 0);
                assert_eq!(b1.stop().seconds, 1000000000 + 6 * 60);
                assert_eq!(b1.stop().nano, 0);
            }
            Err(_) => panic!("This append should not fail"),
        }
    }

    #[test]
    fn test_buffer_time_iter_mtrend()
    {
        let b1 = sample_buffer1_mtrend();
        let vals:Vec<GPSTime> = b1.time_iter().collect();
        let mut expected = Vec::new();
        expected.push(GPSTime{seconds: 1000000000, nano: 0});
        expected.push(GPSTime{seconds: 1000000060, nano: 0});
        expected.push(GPSTime{seconds: 1000000120, nano: 0});
        assert_eq!(vals, expected);
    }

    #[test]
    fn test_buffer_time_iter_raw()
    {
        let b1 = sample_buffer1_raw();
        let vals:Vec<GPSTime> = b1.time_iter().collect();
        let mut expected = Vec::new();
        let mut nano = 0;
        for _i in 0..16u64 {
            expected.push(GPSTime{seconds: 1000000000, nano });
            nano += 1000000000/16;
        }
        nano = 0;
        for _i in 0..16u64 {
            expected.push(GPSTime{seconds: 1000000001, nano });
            nano += 1000000000/16;
        }
        assert_eq!(vals, expected);
    }
}
