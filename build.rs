pub fn main() {
    cxx_build::bridge("src/lib.rs")
        .file("src/nds_client.cc")
        .flag_if_supported("-std=c++14")
        .include("/usr/include/nds2-client")
        .compile("nds_client");

    println!("cargo:rerun-if-changed=src/lib.rs");
    println!("cargo:rerun-if-changed=src/nds_client.cc");
    println!("cargo:rerun-if-changed=include/nds_client.hh");
    // Add instructions to link to any C++ libraries you need.
    println!("cargo:rustc-link-lib=ndscxx");
}